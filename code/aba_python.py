# ABA stands for 'Added Buffer Appraoch'
# A module for analyzing calcium concentration measurements
# with Fura-2
import numpy as np
import matplotlib.pylab as plt

class MultiTS:
    """A multi-dimensional time series"""
    def __init__(self,data,times,
                 time_unit="s",y_units=""):
        """Creates a new multidimensional time series instance.
        
        Parametes
        ---------
        data: a list or numpy array containing the measurements
        times: a list or array containing the measurements times
        time_unit: the time units (a string)
        y_units: a string vector with the units of the columns of data
        """
        self._data = np.array(data)
        self._time = times
        self._time_unit = time_unit
        if len(self._data.shape) == 1: ## only one serie
            self._nobs = self._data.shape[0]
            self._nseries = 1
            self._data.shape = (1,self._nobs)
        else: ## many series
            self._nseries = self._data.shape[0]
            self._nobs = self._data.shape[1]
        if type(y_units) == str and self._nseries > 1:
            self._y_units = [y_units] * self._nseries
        elif len(y_units) < self._nseries:
            self._y_units = (y_units*self._nseries)[:self._nseries]
            self._y_units = y_units
        else:
            self._y_units = y_units
    @property
    def shape(self):
        """Return the shape of the series."""
        return self._data.shape
    @property
    def time(self):
        """Return the time vector."""
        return self._time
    def __getitem__(self,i):
        """Return ith row of the series."""
        if self._nseries == 1:
            return self
        else:
            return MultiTS(self._data[i,:],
                           times = self.time,
                           time_unit = self._time_unit,
                           y_units = self._y_units[i])
    def plot(self,series_idx,**keywords):
        """Plot series_idx row"""
        import matplotlib.pyplot as plt
        X = self.time
        Y = self._data[series_idx,:]
        plt.plot(X,Y,**keywords)
        if "xlabel" not in keywords:
            xlabel = "Time ("+self._time_unit+")"
            plt.xlabel(xlabel)
    def _fns(self,series_idx):
        """Return Five Number Summary of row series_idx."""
        Y = self._data[series_idx,:]
        res = {'min':np.min(Y),
               'qtl1':np.quantile(Y,0.25),
               'median':np.median(Y),
               'qtl3':np.quantile(Y,0.75),
               'max':np.max(Y)}
        return res
    def serie_summary(self,series_idx,ndigits=4):
        """Return Five Number Summary of row series_idx as a string."""
        fns = self._fns(series_idx)
        fns = {nm: np.round(fns[nm],ndigits) for nm in fns.keys()}
        n = ndigits
        wd = max(8,n+2)
        msg = "Serie "+str(series_idx)+" FNS:\n"
        msg += " "*(wd-1)+"Min"
        msg += " "*(wd-5)+"1st Qtl"
        msg += " "*(wd-5)+"Median"
        msg += " "*(wd-6)+"3rd Qtl"
        msg += " "*(wd-2)+"Max\n"
        msg += " {"+"min:>{0}f".format(wd)+"}"
        msg += " {"+"qtl1:>{0}f".format(wd)+"}"
        msg += " {"+"median:>{0}f".format(wd)+"}"
        msg += " {"+"qtl3:>{0}f".format(wd)+"}"
        msg += " {"+"max:>{0}f".format(wd)+"}\n"
        return msg.format(**fns)
    def __str__(self):
        """Produce string representation of object."""
        msg = "A MultiTS (multi-dimensional time-series) object:\n"
        msg += "  number of observations: {0:d}, from {1:f} to {2:f} ({3})\n".\
            format(self._nobs,
                   self.time[0],
                   self.time[-1],
                   self._time_unit)
        msg += "  number of series: {0:d}\n".format(self._nseries)
        for i in range(self._nseries):
            msg += "   " +self.serie_summary(i)
        return msg

class MultiADU(MultiTS):
    """An extension of MultiTS for Calcium related ADU data."""
    @property
    def adu340(self):
        """Return 340 nm measurements from ROI."""
        return self._data[0,:]
    @property
    def adu340B(self):
        """Return 340 nm measurements from ROB (background)."""
        return self._data[1,:]
    @property
    def adu360(self):
        """Return 360 nm measurements from ROI."""
        return self._data[2,:]
    @property
    def adu360B(self):
        """Return 360 nm measurements from ROB (background)."""
        return self._data[3,:]
    @property
    def adu380(self):
        """Return 380 nm measurements from ROI."""
        return self._data[4,:]
    @property
    def adu380B(self):
        """Return 380 nm measurements from ROB (background)."""
        return self._data[5,:]
    def plot(self, what="340",**keywords):
            """Plot one of the six series of the object.
    
            Parameters
            ----------
            what: a string, one of '340', '340B', '360', '360B'
                  '380', '380B'
            **keywords: named parameters passed to pyplot 'plot' method.
            """
            if what == "340":
                    super().plot(0,**keywords)
            elif what == "340B":
                    super().plot(1,**keywords)
            elif what == "360":
                    super().plot(2,**keywords)
            elif what == "360B":
                    super().plot(3,**keywords)
            elif what == "380":
                    super().plot(4,**keywords)
            elif what == "380B":
                    super().plot(5,**keywords)
            else:
                    raise ValueError("Wrong value for 'what'.")
            if "ylabel" not in keywords:
                    ylabel = "ADU_"+what
                    plt.ylabel(ylabel)
    def scatter(self,
                what="340",
                **keywords):
        """Make a scatter plot of one of the six series of the object.
    
            Parameters
            ----------
            what: a string, one of '340', '340B', '360', '360B'
                  '380', '380B'
            **keywords: named parameters passed to pyplot 'scatter' method.
        """
            
        X = self.time
        if what == "340":
            Y = self._data[0,:]
        elif what == "340B":
            Y = self._data[1,:]
        elif what == "360":
            Y = self._data[2,:]
        elif what == "360B":
            Y = self._data[3,:]
        elif what == "380":
            Y = self._data[4,:]
        elif what == "380B":
            Y = self._data[5,:]
        else:
            raise ValueError("Wrong value for 'what'.")
        import matplotlib.pyplot as plt
        plt.scatter(X,Y,**keywords)
        if "xlabel" not in keywords:
            xlabel = "Time ("+self._time_unit+")"
            plt.xlabel(xlabel)
        if "ylabel" not in keywords:
            ylabel = "ADU_"+what
            plt.ylabel(ylabel)        
    def stabilize(self, gain, s2_readout,
                  nb_pixels, nb_pixels_background):
        """Stabilize the variance of the 6 ADU series.
    
        Returns Z = 2*sqrt(ADU/G + nb_pixels*sigma_ro^2)
    
        Parameters
        ----------
        gain: the CCD chip gain
        s2_s2_readout: the CCD chip read-out variance
        nb_pixels: the number of (binned) pixels in the ROI
        nb_pixels_background: the number of binned pixels in the 
                              'background' ROI
    
        Return
        ------
        A MultiTS instance with the stabilized time-series.
        """
        res = np.zeros(self._data.shape)
        res[0,:] = 2*np.sqrt(self.adu340/gain+nb_pixels*s2_readout)
        res[1,:] = 2*np.sqrt(self.adu340B/gain+nb_pixels_background*s2_readout)
        res[2,:] = 2*np.sqrt(self.adu360/gain+nb_pixels*s2_readout)
        res[3,:] = 2*np.sqrt(self.adu360B/gain+nb_pixels_background*s2_readout)
        res[4,:] = 2*np.sqrt(self.adu380/gain+nb_pixels*s2_readout)
        res[5,:] = 2*np.sqrt(self.adu380B/gain+nb_pixels_background*s2_readout)
        return MultiTS(data=res,
                       times=self.time,
                       time_unit=self._time_unit,
                       y_units="Stabilized ADU")
    def Ca_hat(self, T_340, T_380, nb_pixels, nb_pixels_background,
               K_eff, R_min, R_max,
               gain = None, s2_readout = None, rng = None,
               nb_replicate=1000):
        """Return ratiometric [Ca2+] estimator with (option) standard error.
    
        r_hat = (ADU340/P-ADU340B/P_B)/(ADU380/P-ADU380B/P_B)*T_380/T_340
        Ca_hat = K_eff*(r_hat-R_min)/(R_max-r_hat)
    
        If gain, s2_readout and rng are specified, the standard error of Ca_hat
        is estimated and returned as the last time series of the resulting
        MultiTS, otherwise only Ca_hat is computed.
    
        Parameters
        ----------
        T_340: a float with the illumination duration at 340 nm
        T_380: a float with the illumination duration at 380 nm
        nb_pixels: the number of (binned) pixels in the ROI
        nb_pixels_background: the number of binned pixels in the 
                              'background' ROI
        K_eff: parameter K_eff (see above)
        R_min: parameter R_min (see above)
        R_max: parameter R_max (see above)
        gain: the CCD chip gain (default 'None')
        s2_readout: the CCD chip read-out variance (default 'none')
        rng: a 'numpy' random number generator
             (https://numpy.org/doc/stable/reference/random/index.html)
        nb_replicate: positive integer, the number of replicates used for
                      the Monte-Carlo simulation leading to the SE.
    
        Return
        ------
        A MultiTS instance with a single time series (Ca_hat) if the CCD
        parameters (gain, etc) are not specified and with the addition of
        the standard error time series otherwise.
        """
        ADU_340 = self.adu340
        ADU_340B = self.adu340B
        ADU_380 = self.adu380
        ADU_380B = self.adu380B
        if gain != None:
            s_340 = np.sqrt(ADU_340/gain+nb_pixels*s2_readout)*gain
            s_340B = np.sqrt(ADU_340B/gain+nb_pixels_background*s2_readout)*gain
            s_380 = np.sqrt(ADU_380/gain+nb_pixels*s2_readout)*gain
            s_380B = np.sqrt(ADU_380B/gain+nb_pixels_background*s2_readout)*gain
        def _get_r_hat(ADU340,ADU340B,ADU380,ADU380B,T_340,T_380,P,P_B):
            return T_380/T_340*(ADU340/P-ADU340B/P_B)/\
                (ADU380/P-ADU380B/P_B)
        def _get_Ca_hat(r_hat,K_eff,R_min,R_max):
            return K_eff*(r_hat-R_min)/(R_max-r_hat)
        r_hat = _get_r_hat(ADU_340,ADU_340B,
                           ADU_380,ADU_380B,
                           T_340,T_380,
                           nb_pixels,
                           nb_pixels_background)
        if gain != None:
            res = np.zeros((2,self._nobs))
        else:
            res = np.zeros((1,self._nobs))
        res[0,:] = _get_Ca_hat(r_hat,K_eff,R_min,R_max)
        if gain != None:
            sim = np.zeros((nb_replicate,self._nobs))
            for rep_idx in range(nb_replicate):
                r_sim = _get_r_hat(ADU340 = rng.normal(ADU_340,s_340),
                                   ADU340B = rng.normal(ADU_340B,s_340B),
                                   ADU380 = rng.normal(ADU_380,s_380),
                                   ADU380B = rng.normal(ADU_380B,s_380B),
                                   T_340 = T_340, T_380 = T_380,
                                   P = nb_pixels,
                                   P_B = nb_pixels_background)
                sim[rep_idx,:] =  _get_Ca_hat(r_sim,K_eff,R_min,R_max)
            #pdb.set_trace()    
            res[1,:] = sim.std(axis=0)
        return MultiTS(data=res,
                       times=self.time,
                       time_unit=self._time_unit,
                       y_units="mM/1000")
    def phi_hat(self, T340, T360, T380,
                nb_pixels, nb_pixels_background,
                K_eff, R_min, R_max, K_d):
        """Return phi_hat converting the (ADU360,ADU360B) series into F_360 estimates.
    
        Parameters
        ----------
        T_340: a float with the illumination duration at 340 nm
        T_360: a float with the illumination duration at 360 nm
        T_380: a float with the illumination duration at 380 nm
        nb_pixels: the number of (binned) pixels in the ROI
        nb_pixels_background: the number of binned pixels in the 
                              'background' ROI
        K_eff: parameter K_eff (see above in microM)
        R_min: parameter R_min (see above)
        R_max: parameter R_max (see above)
        K_d: Fura dossociation constant (microM)
    
        Return
        ------
        Phi_hat.
        """
        # When we write fluorescence estimator we mean fluorescence
        # per pixel per time unite
        A_340 = self.adu340
        A_340B = self.adu340B
        A_360 = self.adu360
        A_360B = self.adu360B
        A_380 = self.adu380
        A_380B = self.adu380B
        # auto-fluorescence at 340 nm estimator
        S_340_hat = A_340B/nb_pixels_background/T340
        # fluo at 340 nm estimator
        F_340_hat = (A_340/nb_pixels-A_340B/nb_pixels_background)/T340
        # fluo at 360 nm estimator
        F_360_hat = (A_360/nb_pixels-A_360B/nb_pixels_background)/T360
        # auto-fluorescence at 380 nm estimator
        S_380_hat = A_380B/nb_pixels_background/T380
        # fluo at 380 nm estimator
        F_380_hat = (A_380/nb_pixels-A_380B/nb_pixels_background)/T380 
        R_hat = F_340_hat / F_380_hat # Ratio estimator
        Ca_hat = K_eff*(R_hat-R_min)/(R_max-R_hat) # [Ca2+] estimator
        K340 = (R_min*K_eff+R_max*Ca_hat)/(K_d+Ca_hat)
        K380 = (K_eff+Ca_hat)/(K_d+Ca_hat)
        Y340 = A_340/T340/nb_pixels-S_340_hat
        Y380 = A_380/T380/nb_pixels-S_380_hat
        numerator = sum(K340*Y340+K380*Y380)
        denominator = sum((K340**2+K380**2)*F_360_hat)
        return numerator/denominator
    def adu_plug_in(self, T_340, T_360, T_380,
                    nb_pixels, nb_pixels_background,
                    K_eff, R_min, R_max, K_d,
                    phi = None):
        """Return Plug-in estimators of ADU_340 and ADU_380
        
        Parameters
        ----------
        T_340: a float with the illumination duration at 340 nm
        T_360: a float with the illumination duration at 360 nm
        T_380: a float with the illumination duration at 380 nm
        nb_pixels: the number of (binned) pixels in the ROI
        nb_pixels_background: the number of binned pixels in the 
                              'background' ROI
        K_eff: parameter K_eff (see above in microM)
        R_min: parameter R_min (see above)
        R_max: parameter R_max (see above)
        K_d: Fura dossociation constant (microM)
        phi: An estimate of phi, if 'None' (default), it is computed
             from the data.
    
        Return
        ------
        A MultiTS instance with the plug-in estimates of ADU_340 and ADU_380.
        """
        if phi == None:
            phi = self.phi_hat(T_340,T_360,T_380,nb_pixels,nb_pixels_background,
                               K_eff,R_min,R_max,K_d)
        A_360 = self.adu360
        A_360B = self.adu360B
        F_360_hat = (A_360/nb_pixels-A_360B/nb_pixels_background)/T_360
        Ca = self.Ca_hat(T_340, T_380, nb_pixels,
                         nb_pixels_background, K_eff, R_min, R_max)
        factor = F_360_hat*phi/(K_d+Ca._data[0,:])
        res = np.zeros((2,len(A_360)))
        A_340B = self.adu340B
        S_B340 = A_340B/nb_pixels_background/T_340
        res[0,:] = ((R_min*K_eff+R_max*Ca._data[0,:])*factor+S_B340)*T_340*nb_pixels
        A_380B = self.adu380B
        S_B380 = A_380B/nb_pixels_background/T_380
        res[1,:] = ((K_eff+Ca._data[0,:])*factor+S_B380)*T_380*nb_pixels
        return MultiTS(data=res,
                       times=self.time,
                       time_unit=self._time_unit,
                       y_units="Stabilized ADU")
    def fit_ratiometric(self, T_340, T_380,
                        nb_pixels, nb_pixels_background,
                        K_eff, R_min, R_max,
                        gain, s2_readout,
                        rng, nb_replicate = 1000,
                        baseline = 7,
                        fit_from=0.5):
        """Estimate mono-exponential paremters from linearized N&H (1992) model.
    
        A mono-exponential fit is performed on the decaying pahse of a [Ca2+]
        transient. [Ca2+] is estimated with the ratiometric estimator and the
        corresponding standard error is obtained by Monte-Carlo. A non-linear 
        least-squares fit is used with the Levenberg-Marquardt method provided
        by function least_squares of scipy.optimize.
    
        The baseline length is set by parameter 'baseline'
        Parameter 'fit_from' sets the location from which the dacaying part
        of the transient is fitted. If 'fit_from' >= 1 it is intrpreted as the
        number of sample points after the transient peak from which the fit is 
        started; If 0 < 'fit_from' < 1 it is intepreted as the fraction of the 
        transient induced [Ca2+] increase after which the fit is started.
    
        Parameters
        ----------
        T_340: a float with the illumination duration at 340 nm
        T_380: a float with the illumination duration at 380 nm
        nb_pixels: the number of (binned) pixels in the ROI
        nb_pixels_background: the number of binned pixels in the 
                              'background' ROI
        K_eff: parameter K_eff
        R_min: parameter R_min
        R_max: parameter R_max
        gain: the CCD chip gain
        s2_readout: the CCD chip read-out variance
        rng: a 'numpy' random number generator
             (https://numpy.org/doc/stable/reference/random/index.html)
        nb_replicate: positive integer, the number of replicates used for
                      the Monte-Carlo simulation leading to the SE.
        baseline: baseline length (see above)
        fit_from: the location from which the dacay pahes is fitted (see
                  above).
    
        Return
        ------
        A RatioFitResult instance.
    
        Side Effect
        -----------
        Fit information and results are printed to the standard output.
        """
        Ca = self.Ca_hat(T_340, T_380, nb_pixels, nb_pixels_background,
                         K_eff, R_min, R_max, gain, s2_readout, rng,
                         nb_replicate)
        ratio_Ca = Ca._data[0,:]
        ratio_time = Ca.time.copy()
        ratio_se = Ca._data[1,:]
        n = len(ratio_Ca)
        if fit_from < 1:
            base_value = np.mean(ratio_Ca[:baseline])
            delta = np.max(ratio_Ca)-base_value
            Ca_rectified = ratio_Ca.copy()
            Ca_rectified[Ca_rectified > base_value+0.5*delta] = base_value
            fit_start = np.argmax(Ca_rectified)
        else:
            peak_pos = np.argmax(ratio_Ca)
            fit_start = peak_pos+fit_from
        good_idx = list(range(baseline))+list(range(fit_start,n))
        Ca = ratio_Ca[good_idx]
        sigma = ratio_se[good_idx]
        tt = ratio_time[fit_start:]
        tt = tt-tt[0]
        n_obs = len(Ca)
        def residual(par):
            Ca0 = par[0]
            delta = par[1]
            tau = par[2]
            res = np.zeros((n_obs,))
            res[:baseline] = (Ca[:baseline]-Ca0)/sigma[:baseline]
            res[baseline:] = (Ca[baseline:]-Ca0-delta*np.exp(-tt/tau))/sigma[baseline:]
            return res
        Ca0_0 = np.mean(Ca[:baseline])
        delta_0 = Ca[baseline]-Ca0_0
        tau_0 = tt[-1]/5
        par0 = np.array([Ca0_0,delta_0,tau_0])
        from scipy.optimize import least_squares
        fit = least_squares(residual,par0,verbose=1,method='lm')
        inv_hessian = np.linalg.inv(np.dot(np.transpose(fit.jac),fit.jac))
        par_se = np.sqrt(np.diag(inv_hessian))
        res = RatioFitResult(theta_hat = fit.x,
                             theta_se = par_se,
                             baseline = baseline,
                             start_fit = fit_start,
                             nobs = n_obs,
                             rss = fit.cost*2,
                             Ca_hat = ratio_Ca,
                             Ca_se = ratio_se,
                             Ca_time = ratio_time)
        print(str(res))
        return res  
    def adu_summary(self,series_idx):
        """Return Five Number Summary of row series_idx as a string."""
        res = self._fns(series_idx)
        n = int(np.ceil(np.log(res['max'])))
        wd = max(8,n+2)
        what = ["340","340B","360","360B","380","380B"]
        msg = "ADU_"+what[series_idx]+" summary:\n"
        msg += " "*(wd-4)+"Min"
        msg += " "*(wd-8)+"1st Qtl"
        msg += " "*(wd-7)+"Median"
        msg += " "*(wd-8)+"3rd Qtl"
        msg += " "*(wd-4)+"Max\n"
        msg += " {"+"min:{0}d".format(wd-2)+"}"
        msg += " {"+"qtl1:{0}.{1}f".format(wd-2,2)+"}"
        msg += " {"+"median:{0}.{1}f".format(wd-2,2)+"}"
        msg += " {"+"qtl3:{0}.{1}f".format(wd-2,2)+"}"
        msg += " {"+"max:{0}d".format(wd-2)+"}\n"
        return msg.format(**res)
    def __str__(self):
        """Produce string representation of object."""
        msg = "A MultiADU (multi-dimensional ADU-series) object:\n"
        msg += "  number of observations: {0:d}, from {1:f} to {2:f} ({3})\n".\
            format(self._nobs,
                   self.time[0],
                   self.time[-1],
                   self._time_unit)
        for i in range(self._nseries):
            msg += "   " +self.adu_summary(i)
        return msg

class RatioFitResult:
    """Monoexponential fit to ratiometric data."""
    def __init__(self, theta_hat, theta_se,
                 baseline, start_fit, nobs,
                 rss, Ca_hat, Ca_se, Ca_time):
        """Creates a new RatioFitResult instance.
        
        Parameters
        ----------
        theta_hat: array of estimated model parameters
        theta_se: array of standard errors of 'theta_hat'
        baseline: paramter with that name used in fitting procedure
        start_fit: sample point from which the decay part of the transient
                   was fitted.
        nobs: number of observations used
        rss: residual sum of squares
        Ca_hat: fitted measurements
        Ca_se: standard errors on measurements
        Ca_time: array of time values for 'Ca_hat' and 'Ca_se'
        """
        Ca0 = theta_hat[0]
        self.Ca0_hat = Ca0
        self.Ca0_se = theta_se[0]
        delta = theta_hat[1]
        self.delta_hat = delta
        self.delta_se = theta_se[1]
        tau = theta_hat[2]
        self.tau_hat = tau
        self.tau_se = theta_se[2]
        self.baseline = baseline
        self.start_fit = start_fit
        self.nobs = nobs
        self.rss = rss
        self.Ca = Ca_hat
        self.SE = Ca_se
        self.time = Ca_time
        self.t_early = Ca_time[:baseline]
        self.t_late = Ca_time[start_fit:]
        self.pred_early = self.predict(self.t_early)
        pred_late = self.predict(self.t_late)
        self.pred_late = pred_late
        self.res_early = (Ca_hat[:baseline] - Ca0)/Ca_se[:baseline]
        self.res_late = (Ca_hat[start_fit:]-pred_late)/Ca_se[start_fit:]
    def predict(self,t):
        """Return fitted value at t."""
        Ca0 = self.Ca0_hat
        t0 = self.t_late[0]
        delta = self.delta_hat
        tau = self.tau_hat
        return Ca0+np.where(t>=t0,delta*np.exp(-(t-t0)/tau),0)
    def __str__(self):
        """Produce string representation of object."""
        msg = "\nRatiometric estimator mono-exponential fit\n"
        msg += "Fitted model Ca = baseline+delta*exp(-(t-t0)/tau)\n"
        msg += "nobs = {0:d}\n".format(self.nobs)
        msg += "number of degrees of freedom = {0:d}\n".format(self.nobs-3)
        msg += "baseline length = {0:d}\n".format(self.baseline)
        msg += "fit started from point {0:d}\n".format(self.start_fit)
        msg += "estimated baseline {0:f} and standard error {1:f}\n".format(self.Ca0_hat,self.Ca0_se)
        msg += "estimated delta {0:f} and standard error {1:f}\n".format(self.delta_hat,self.delta_se)
        msg += "estimated tau {0:f} and standard error {1:f}\n".format(self.tau_hat,self.tau_se)
        msg += "residual sum of squares: {0:f}\n".format(self.rss)
        msg += "RSS per degree of freedom: {0:f}\n".format(self.rss/(self.nobs-3))
        from scipy.stats import chi2
        p_value = chi2.cdf(self.rss,self.nobs-3)
        msg += "Probability of observing a larger of equal RSS per DOF\n"
        msg += " under the null hypothesis: {0:f}\n".format(1-p_value)
        return msg
    def plot(self,data_color='black',data_lw=1,fit_color='red',fit_lw=1):
        """Plot data, fit and residuals
    
        Parameters
        ----------
        data_color: color used for the data
        data_lw: line width for the data
        fit_color: color used for the fit
        fit_lw: line width for the fit 
        """
        import matplotlib.pyplot as plt
        fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=True)
        ax0.plot(self.t_early,self.res_early,color=fit_color,lw=fit_lw)
        ax0.plot(self.t_late,self.res_late,color=fit_color,lw=fit_lw)
        ax0.set_ylabel('Norm. residuals')
        ax1.errorbar(self.time, self.Ca,
                     yerr=self.SE, color=data_color,
                     lw=data_lw)
        ax1.plot(self.t_early,self.pred_early,color=fit_color,lw=fit_lw)
        ax1.plot(self.t_late,self.pred_late,color=fit_color,lw=fit_lw)
        ax1.set_ylabel(r"[Ca$^{2+}$] ($\mu$M)")
        plt.xlabel("Time (s)")

class FluoData:
    """Storing and analyzing added buffer approach experimants."""
    def __init__(self,data_file_name):
        """Create a new FluoData instance.
    
        This method requires function 'h5' of module 'h5py'
    
        Parameter
        ---------
        data_file_name: name of the HDF5 file containing the data.
        """
        import h5py as h5
        with h5.File(data_file_name,'r') as h5:
            self._gain = h5['CCD']['GAIN'][0]
            self._s2_readout = h5['CCD']['S_RO'][0]**2
            self._nb_pixels = h5['CCD']['P'][0]
            self._nb_pixels_background = h5['CCD']['P_B'][0]
            self._T_340 = h5['ILLUMINATION']['T_340'][0]
            self._T_360 = h5['ILLUMINATION']['T_360'][0]
            self._T_380 = h5['ILLUMINATION']['T_380'][0]
            self._K_eff = h5['DYE']['K_eff_hat'][0]
            self._R_min = h5['DYE']['R_min_hat'][0]
            self._R_max = h5['DYE']['R_max_hat'][0]
            self._K_d = h5['DYE']['K_d_hat'][0]
            self._pipette_concentration = h5['DYE']['pipette_concentration'][0]
            stim_list = list(h5['DATA'].keys())
            def data_xtract(grp):
                TIME_DELTA = grp['TIME_DELTA'][0]
                TIME_OFFSET = grp['TIME_OFFSET'][0]
                TT = TIME_OFFSET+TIME_DELTA*grp['ADU'][...,0]
                ADU_340 = grp['ADU'][...,1]
                ADU_340B = grp['ADU'][...,2]
                ADU_360 = grp['ADU'][...,3]
                ADU_360B = grp['ADU'][...,4]
                ADU_380 = grp['ADU'][...,5]
                ADU_380B = grp['ADU'][...,6]
                return {'data':[ADU_340,ADU_340B,
                                ADU_360,ADU_360B,
                                ADU_380,ADU_380B],
                        'times':TT,
                        'y_units': "ADU"}
            self._data = {identifier: \
                          MultiADU(**data_xtract(h5['DATA'][identifier])) \
                          for identifier in stim_list}
            self._date = h5['EXPERIMENT']['DATE'][...][0].decode()
            self._expname = h5['EXPERIMENT']['EXPNAME'][...][0].decode()
            self._neuron_type = h5['EXPERIMENT']['OBJECT'][...][0].decode()
            self._observer = h5['EXPERIMENT']['OBSERVER'][...][0].decode()
            self._lab = h5['EXPERIMENT']['LAB'][...][0].decode()
    @property    
    def gain(self):
        """CCD chip gain."""
        return self._gain
    @property    
    def s2_readout(self):
        """CCD chip read-out variance."""
        return self._s2_readout
    @property    
    def nb_pixels(self):
        """Number of (binned) pixels in the Region Of Interest (ROI)."""
        return self._nb_pixels
    @property    
    def nb_pixels_background(self):
        """Number of (binned) pixels in the Background Estimation Region."""
        return self._nb_pixels_background
    @property
    def T_340(self):
        """Illumination duration at 340 nm (s)."""
        return self._T_340
    @property
    def T_360(self):
        """Illumination duration at 360 nm (s)."""
        return self._T_360
    @property
    def T_380(self):
        """Illumination duration at 380 nm (s)."""
        return self._T_380
    @property
    def K_eff(self):
        """Parameter K_eff in 10e-6M."""
        return self._K_eff
    @property
    def R_min(self):
        """Parameter R_min (no dimension)."""
        return self._R_min
    @property
    def R_max(self):
        """Parameter R_max (no dimension)."""
        return self._R_max
    @property
    def K_d(self):
        """Parameter K_d (Fura dissociation constant) in 10e-6M."""
        return self._K_d
    @property
    def Fura(self):
        """Fura concentration (in 10e-6M) in the pipette."""
        return self._pipette_concentration
    @property
    def data_keys(self):
        """List of data sets."""
        return list(self._data.keys())
    def __getitem__(self,j):
        """Return sepcific data set."""
        return self._data[j]
    def __str__(self):
        """Produce string representation of FluoData."""
        msg = "Experiment {0} made by {1} ({2}) on {3}.\n".\
            format(self._expname,self._observer,self._lab, self._date)
        msg += "It contains {0:d} data sets:\n".format(len(self.data_keys))
        for dname in self.data_keys:
            msg += " " + dname
        msg += ".\n"
        msg += "The CCD chip parameters are {0:.3f} (gain)".format(self.gain)
        msg += " and {0:.3f} (read-out variance).\n".format(self.s2_readout)
        msg += "{0:d} (binned) pixels were used in the ROI".format(self.nb_pixels)
        msg += " and {0:d} in the background estimation region.\n".\
            format(self.nb_pixels_background)
        msg += "The illumination times were: {0:.3f} (340 nm), ".format(self.T_340)
        msg += "{0:.3f} (360 nm) and {1:.3f} (380 nm) sec.\n".\
            format(self.T_360,self.T_380)
        msg += "The Fura-2 parameters were:\n"
        msg += " K_eff = {0:.3f} (10e-6M), K_d = {1:.3f} (10e-6M),\n".\
            format(self.K_eff,self.K_d)
        msg += " R_min = {0:.3f} and R_max = {1:.3f},\n".\
            format(self.R_min,self.R_max)
        msg += " For a Fura concentration of {0:.2f} (10e-6M).".\
            format(self.Fura)
        return msg
    def plot(self,what="loading",**kargs):
        """Plot loading curve of normalized ratiometric [Ca2+] estimators.
    
        If 'what' is set to 'loading' the loading curve is drawn as a scatter plot.
        What is plotted is the 360 measurements corrected for background and
        divided by illumination time.
    
        If 'what' is set to 'Ca' the normalized ratiometric [Ca2+] estimators
        are drawn. Normalization is done by first subtracting the mean of the
        first 5 measurements and then the transient peak is set to 1. A log
        scale is used on the ordinate.
    
        Parameters
        ----------
        what: either 'loading' (default) or 'Ca'
        """
        known = ["loading","Ca"]
        if not what in known:
            raise ValueError("Unkonw 'what'.")
        if what == "loading":
            self._plot_loading(**kargs)
        if what == "Ca":
            self._plot_Ca(**kargs)
    
    def _plot_loading(self,**kargs):
        npix = self.nb_pixels
        npix_b = self.nb_pixels_background
        y_load = self['load']._data[2,:]/npix - self['load']._data[3,:]/npix_b
        tt = self['load'].time
        y_max = max(y_load)
        y_load *= self.Fura/y_max
        plt.scatter(tt,y_load,**kargs)
        plt.ylabel(r"Estimated [Fura] ($\mu$M)")
        other = [stim for stim in self.data_keys if stim != "load"]
        for stim in other:
            y_360 = self[stim]._data[2,:]/npix - self[stim]._data[3,:]/npix_b
            y_360 *= self.Fura/y_max
            tt = self[stim].time
            plt.scatter(tt,y_360,**kargs)
    
    def _plot_Ca(self,**kargs):
        other = [stim for stim in self.data_keys if stim != "load"]
        Ca_s = [self[stim].Ca_hat(self.T_340,self.T_380,
                                  self.nb_pixels,self.nb_pixels_background,
                                  self.K_eff,self.R_min,self.R_max)
                for stim in self.data_keys if stim != "load"]
        for Ca in Ca_s:
            X = Ca.time.copy()
            X -= X[0]
            Y = Ca._data[0,:]
            Y -= np.mean(Y[:5])
            Y /= max(Y)
            plt.plot(X,Y,**kargs)
        plt.yscale("log")
    def kappaB_from_ratio(self,rng, stim = None, 
                          nb_replicate = 1000, baseline = 7, fit_from = 0.5):
        """Estimate kappa_B with ratiometric estimators and the added-buffer appraoch.
    
        Fits transients from the ratiometric estimators from stimulations
        specified by 'stim' (by default all are used) before fitting the
        tau vs kappa data with a weighted linear fit.
    
        Parameters
        ----------
        rng: a 'numpy' random number generator
             (https://numpy.org/doc/stable/reference/random/index.html)
        stim: a list of indices or names specifying which stimulations
              should be used (by default they are all used).
        nb_replicate: positive integer, the number of replicates used for
                      the Monte-Carlo simulation leading to the SE of the
                      ratiometric estimator..
        baseline: baseline length (see MultiADU.Ca_hat)
        fit_from: the location from which the dacay pahes is fitted 
                  (see MultiADU.Ca_hat).
    
        Return
        ------
        A KappaFitResult instance.
    
        Side Effect
        -----------
        Fit information and results are printed to the standard output.
        """
        stim_all = [stimu for stimu in self.data_keys if stimu != "load"]
        if stim == None:
            stim = stim_all
        elif type(stim[0]) == int:
            stim = [stim_all[s] for s in stim]
        npix = self.nb_pixels
        npix_b = self.nb_pixels_background
        y_load = self['load'].adu360/npix - self['load'].adu360B/npix_b
        tt = self['load'].time
        y_max = max(y_load)
        ratio_results = []
        for s in stim:
            msg = "***********************************\n"
            msg += "*  Doing now stimulation " + s + "\n"
            msg += "***********************************"
            print(msg)
            res = self[s].fit_ratiometric(self.T_340,self.T_380,
                                          npix,npix_b,
                                          self.K_eff,self.R_min,self.R_max,
                                          self.gain,self.s2_readout,rng,
                                          nb_replicate = nb_replicate,
                                          baseline = baseline, fit_from = fit_from)
            fura = (self[s].adu360[res.start_fit:]/npix-\
                    self[s].adu360B[res.start_fit:]/npix_b)
            fura *= self.Fura/y_max
            res_dic = {'fit': res,
                       'kappa_mean': np.mean(fura)*self.K_d/(self.K_d+res.Ca0_hat)**2,
                       'kappa_max': np.max(fura)*self.K_d/(self.K_d+res.Ca0_hat)**2,
                       'kappa_min': np.min(fura)*self.K_d/(self.K_d+res.Ca0_hat)**2}
            msg = "***********************************\n"
            msg += "*  Stimulation " + s + " done\n"
            msg += "***********************************\n"
            print(msg)
            ratio_results.append(res_dic)
        res = TauVsKappa({stim[i]: ratio_results[i] for i in range(len(stim))})
        print(res)
        return res

class TauVsKappa:
    """tau vs kappa fit"""
    def __init__(self,
                 sgl_transient_fits):
        """Create a TauVsKappa instance.
    
        Parameter
        ---------
        sgl_transient_fits: a dictionary whose keys should be 'stim1', 'stim2',
          tec (the transients used). Each element should itself be a dictionary
          with the following keys:
            fit: a Ratiofitresult instance
            kappa_mean: the mean value of the Fura binding capacity during the 
                        falling phase of the transient,
            kappa_max:  the max value of the Fura binding capacity during the 
                        falling phase of the transient,
            kappa_min:  the min value of the Fura binding capacity during the 
                        falling phase of the transient.
        """
        self._stim_used = list(sgl_transient_fits.keys())
        k_mean = [sgl_transient_fits[elt]['kappa_mean'] \
                  for elt in sgl_transient_fits]
        k_max = [sgl_transient_fits[elt]['kappa_max'] \
                 for elt in sgl_transient_fits]
        k_min = [sgl_transient_fits[elt]['kappa_min'] \
                 for elt in sgl_transient_fits]
        tau = [sgl_transient_fits[elt]['fit'].tau_hat \
               for elt in sgl_transient_fits]
        se = [sgl_transient_fits[elt]['fit'].tau_se \
              for elt in sgl_transient_fits]
        fits = {elt:sgl_transient_fits[elt]['fit'] \
                for elt in sgl_transient_fits}
        def _lstsqr(kappa,
                    tau_hat,
                    tau_se):
            """Linear weighted least square fit of tau vs kappa
        
            Parameters
            ----------
            kappa: a list or numpy array with the kappa values.
            tau_hat: a list or numpy array with the estimated tau.
            tau_se: a list or numpy array with the standard errors on tau
            
            Return
            ------
            A dictionary with:
             beta_hat: the estimated parameters
             beta_cov: their covariance matrix
             tss: the total sum of squares
             rss: the residual sum of squares
             p_value: the probability for a chisq to exceed rss under the null
             R2: the R^2 (1-rss/tss)
             gamma_hat: the estimated gamma/v
             gamma_se: the standard error of gamma_hat
             kappa_S_hat: the estimated kappa_S
             kappa_S_se: the standard error of kappa_S_hat
            """
            from numpy import linalg
            n = len(kappa)
            if len(tau_hat) != n or len(tau_se) != n:
                raise ValueError("Data length do not match.")
            y = np.array(tau_hat)
            se = np.array(tau_se)
            w2 = 1/se**2
            mu_w = sum(w2*y)/sum(w2)
            tss = sum(w2*(y-mu_w)**2)
            W = np.diag(1/se)
            x1 = np.array(kappa)
            X = np.vstack([np.ones(n),x1]).T
            yp = np.dot(W,y)
            Xp = np.dot(W,X)
            M_matrix = linalg.inv(np.dot(Xp.T,Xp))
            Hat_matrix = np.dot(M_matrix,Xp.T)
            beta = np.dot(Hat_matrix,yp)
            rss = sum((yp-np.dot(Xp,beta))**2)
            from scipy.stats import chi2
            p_value = 1-chi2.cdf(rss,n-2)
            R_squared = 1-rss/tss
            gamma_o_v_hat = 1/beta[1]
            gamma_o_v_se = np.sqrt(M_matrix[1,1])/beta[1]**2
            kappa_S_hat = beta[0]/beta[1]-1
            kappa_S_se = np.sqrt(M_matrix[0,0]/beta[1]**2+\
                                 M_matrix[1,1]*beta[0]**2/beta[1]**4)
            return {'beta_hat':beta,
                    'beta_cov':M_matrix,
                    'tss':tss,
                    'rss':rss,
                    'p_value':p_value,
                    'R2': R_squared,
                    'gamma_hat':gamma_o_v_hat,
                    'gamma_se':gamma_o_v_se,
                    'kappa_S_hat':kappa_S_hat,
                    'kappa_S_se':kappa_S_se}
        self._fit_mean = _lstsqr(k_mean,tau,se)
        self._fit_max = _lstsqr(k_max,tau,se)
        self._fit_min = _lstsqr(k_min,tau,se)
        self._k_mean = k_mean
        self._k_max = k_max
        self._k_min = k_min
        self._tau_hat = tau
        self._tau_se = se
        self._fits = fits
    def _summary(self,what):
        """string representation of a tau vs kappa fit.
    
        Parameter
        ---------
        what: one of 'mean', 'min' or 'max'
    
        Return
        ------
        A string
        """
        allowed_what = ["mean","min","max"]
        if not what in allowed_what:
            raise ValueError("Wrong 'what' value.")
        msg = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"+\
            "tau vs kappa weighted linear fit using\n "+\
            what+" Fura value during transient falling phase.\n"
        if what == "mean":
            fit = self._fit_mean
        elif what == "max":
            fit = self._fit_max
        else:
            fit = self._fit_min
        msg += "Best fit: tau = {0:.3e} + {1:.3e} kappa_Fura\n".\
            format(fit['beta_hat'][0],fit['beta_hat'][1])
        msg += "Covariance matrix:\n"
        msg += "[ {0:.3e}, {1:.3e}\n".format(fit['beta_cov'][0,0],
                                             fit['beta_cov'][0,1])
        msg += "  {0:.3e}, {1:.3e} ]\n".format(fit['beta_cov'][1,0],
                                               fit['beta_cov'][1,1])
        msg += "Total sum of squares (TSS) = {0:.2e}\n".format(fit['tss'])
        msg += "Residual sum of squares (RSS) = {0:.2e}\n".format(fit['rss'])
        msg += "Probability of observing a larger of equal RSS per DOF\n"
        msg += " under the null hypothesis: {0:.3e}\n".format(fit['p_value'])
        msg += "R squared (1-RSS/TSS) = {0:.3e}\n".format(fit['R2'])
        msg += "Estimated gamma/v with standard error: {0:.3e}".format(fit['gamma_hat'])
        msg += " +/- {0:.3e}\n".format(fit['gamma_se'])
        msg += "Estimated kappa_S with standard error (using error propagation):\n"
        msg += " {0:.3e} +/- {1:.3e}\n".format(fit['kappa_S_hat'],
                                               fit['kappa_S_se'])
        msg += "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
        return msg
    def _lstsqr(kappa,
                tau_hat,
                tau_se):
        """Linear weighted least square fit of tau vs kappa
    
        Parameters
        ----------
        kappa: a list or numpy array with the kappa values.
        tau_hat: a list or numpy array with the estimated tau.
        tau_se: a list or numpy array with the standard errors on tau
        
        Return
        ------
        A dictionary with:
         beta_hat: the estimated parameters
         beta_cov: their covariance matrix
         tss: the total sum of squares
         rss: the residual sum of squares
         p_value: the probability for a chisq to exceed rss under the null
         R2: the R^2 (1-rss/tss)
         gamma_hat: the estimated gamma/v
         gamma_se: the standard error of gamma_hat
         kappa_S_hat: the estimated kappa_S
         kappa_S_se: the standard error of kappa_S_hat
        """
        from numpy import linalg
        n = len(kappa)
        if len(tau_hat) != n or len(tau_se) != n:
            raise ValueError("Data length do not match.")
        y = np.array(tau_hat)
        se = np.array(tau_se)
        w2 = 1/se**2
        mu_w = sum(w2*y)/sum(w2)
        tss = sum(w2*(y-mu_w)**2)
        W = np.diag(1/se)
        x1 = np.array(kappa)
        X = np.vstack([np.ones(n),x1]).T
        yp = np.dot(W,y)
        Xp = np.dot(W,X)
        M_matrix = linalg.inv(np.dot(Xp.T,Xp))
        Hat_matrix = np.dot(M_matrix,Xp.T)
        beta = np.dot(Hat_matrix,yp)
        rss = sum((yp-np.dot(Xp,beta))**2)
        from scipy.stats import chi2
        p_value = 1-chi2.cdf(rss,n-2)
        R_squared = 1-rss/tss
        gamma_o_v_hat = 1/beta[1]
        gamma_o_v_se = np.sqrt(M_matrix[1,1])/beta[1]**2
        kappa_S_hat = beta[0]/beta[1]-1
        kappa_S_se = np.sqrt(M_matrix[0,0]/beta[1]**2+\
                             M_matrix[1,1]*beta[0]**2/beta[1]**4)
        return {'beta_hat':beta,
                'beta_cov':M_matrix,
                'tss':tss,
                'rss':rss,
                'p_value':p_value,
                'R2': R_squared,
                'gamma_hat':gamma_o_v_hat,
                'gamma_se':gamma_o_v_se,
                'kappa_S_hat':kappa_S_hat,
                'kappa_S_se':kappa_S_se}
    def __getitem__(self,j):
        """Return sepcific transient fit."""
        return self._fits[j]
    def __str__(self):
        return self._summary("min") + self._summary("mean") + self._summary("max")
    def plot(self,what="mean",**kargs):
        """Plot tau vs kappa linear fit results.
    
        Parameters
        ----------
        what: what: one of 'mean', 'min' or 'max'
        """
        allowed_what = ["mean","min","max"]
        if not what in allowed_what:
            raise ValueError("Wrong 'what' value.")
        if what == "mean":
            fit = self._fit_mean
            kappa_F = np.array(self._k_mean)
            xlabel = r"Mean $\kappa_{Fura}$"
        elif what == "max":
            fit = self._fit_max
            kappa_F = np.array(self._k_max)
            xlabel = r"Max $\kappa_{Fura}$"
        else:
            fit = self._fit_min
            kappa_F = np.array(self._k_min)
            xlabel = r"Min $\kappa_{Fura}$"
        kappa_S_hat = fit['kappa_S_hat']
        kappa_S_se = fit['kappa_S_se']
        beta_hat = fit['beta_hat']
        beta_cov = fit['beta_cov']
        tau_hat = np.array(self._tau_hat)
        tau_se = np.array(self._tau_se)
        kappa_bd = 0
        if kappa_S_hat > 0:
            kappa_bd = 1.25*kappa_S_hat+1
        kappa_range = 1.05*kappa_F[-1]+kappa_bd
        delta_kappa = kappa_range/250
        kappaP = -kappa_bd+delta_kappa*np.arange(250)
        tauP = beta_hat[0]+beta_hat[1]*kappaP
        tauP_se = np.array([np.sqrt(np.dot([1,k],np.dot(beta_cov,[1,k]))) \
                            for k in kappaP])
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots()
        ax.errorbar(kappa_F, tau_hat, yerr=tau_se*1.96, fmt='o', color='red')
        plt.plot(kappaP,tauP,color='blue')
        plt.plot(kappaP,tauP+1.96*tauP_se,color='black',linestyle='dashed')
        plt.plot(kappaP,tauP-1.96*tauP_se,color='black',linestyle='dashed')
        plt.xlabel(xlabel)
        plt.ylabel(r"Decay time $\tau$ (s)")
