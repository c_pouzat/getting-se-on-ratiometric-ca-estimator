% Created 2021-05-26 Wed 15:27
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{cmbright}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{graphicx,longtable,url,rotating}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{subfig}
\usepackage{minted}
\usepackage{algpseudocode}
\usepackage{alltt}
\usepackage{pdfpages}
\usepackage[backend=biber,style=numeric,isbn=false,url=true,eprint=true,doi=false]{biblatex}
\renewenvironment{verbatim}{\begin{alltt} \scriptsize \color{Bittersweet} \vspace{0.2cm} }{\vspace{0.2cm} \end{alltt} \normalsize \color{black}}
\addbibresource{methodsx.bib}
\definecolor{lightcolor}{gray}{.55}
\definecolor{shadecolor}{gray}{.95}

                 \usepackage{hyperref}
                 \hypersetup{colorlinks=true,urlcolor=orange}
\author{Simon Hess\(^{1\star}\), Christophe Pouzat\(^{2\star}\), Peter Kloppenburg\(^{1}\)}
\date{\today}
\title{A Simple Method for Getting Standard Error on the Ratiometric Calcium Estimator}
\hypersetup{
 pdfauthor={Simon Hess\(^{1\star}\), Christophe Pouzat\(^{2\star}\), Peter Kloppenburg\(^{1}\)},
 pdftitle={A Simple Method for Getting Standard Error on the Ratiometric Calcium Estimator},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.3.1 (Org mode 9.0.9)}, 
 pdflang={English}}
\begin{document}
\includepdf[pages=-]{MethodsX_Hess_Pouzat_Kloppenburg_template.pdf}
\maketitle
\section*{Affiliations}

$^{1}$\textbf{Simon Hess and Peter Kloppenburg} \\
Institute for Zoology \\
Biocenter and Cologne Excellence Cluster in Aging Associated Diseases (CECAD)\\
University of Cologne\\
Cologne\\
Germany\\
\vspace{0.1cm}
\\
$^{2}$ \textbf{Christophe Pouzat}\\
IRMA\\
Strasbourg University and CNRS UMR 7501\\
Strasbourg\\
France\\
\\
$^{\star}$ Simon Hess and Christophe Pouzat contributed equally
\vspace{0.15cm}
\\
\textbf{Correspondance should be addressed to}:\\
C. Pouzat\\
IRMA\\
7 rue René-Descartes\\
67084 Strasbourg Cedex\\
France\\
christophe.pouzat@math.unistra.fr\\
\vspace{0.1cm}
\\
\textbf{Keywords}: Calcium imaging, Fluorescence measurements, error propagation, Python, reproducible research
\vspace{0.15cm}
\\
\textbf{Co-submission}: This manuscript is a co-submission associated with \textit{Analysis of neuronal Ca$^{2+}$ handling properties by combining perforated patch clamp recordings and the added buffer approach}, DOI: 10.1016/j.ceca.2021.102411.
\pagebreak

\section{Abstract}
\label{sec:org15e04b5}

The ratiometric fluorescent calcium indicator Fura-2 plays a fundamental role in the investigation of cellular calcium dynamics. Despite of its widespread use in the last 30 years, only one publication \cite{Joucla_2010} proposed a way of obtaining confidence intervals on fitted calcium dynamic model parameters from single 'calcium transients'. Shortcomings of this approach are its requirement for a '3 wavelengths' protocol (excitation at 340 and 380 nm as usual plus at 360 nm, the isosbectic point) as well as the need for an autofluorence / background fluorescence model at each wavelength. We propose here a simpler method that eliminates both shortcommings:
\begin{enumerate}
\item a precise estimation of the standard errors of the raw data is obtained first,
\item the standard error of the ratiometric calcium estimator (a function of the raw data values) is derived using both the propagation of uncertainty and a Monte-Carlo method.
\end{enumerate}
Once meaningful standard errors for calcium estimates are available, standard errors on fitted model parameters follow directly from the use of nonlinear least-squares optimization algorithms.

\section{Graphical abstract}
\label{sec:org4ba0da2}

\begin{center}
\includegraphics[width=0.9\textwidth]{figs/Summary_fig.png}
\captionof{figure}{\label{fig:Summary} How to get error bars on the ratiometric calcium estimator? The figure is to be read clockwise from the bottom right corner. The two measurements areas (region of interest, ROI, on the cell body and background measurement region, BMR, outside of the cell) are displayed on the frame corresponding to one actual experiment. Two measurements, one following an excitation at 340 nm and the other following an excitation at 380 nm are performed (at each 'time point') from each region. The result is a set of four measures: adu340 (from the ROI), adu340B (from the BMR), adu380 and adu380B. \emph{These measurements are modeled as realizations of Gaussian random variables}. The fact that the measurements as well as the subsequent quantities derived from them are random variable realization is conveyed throughout the figure by the use of Gaussian probability densities. The densities from the MRB are 'tighter' because there are much more pixels in the MRB than in the ROI (the standard deviations of the densities shown on this figure have been enlarged for clarity, but their relative size has been preserved, the horizontal axis in black always starts at 0). The key result of the paper is that the standard deviation of the four Gaussian densities corresponding to the raw data (bottom of the figure) can be reliably estimated from the data alone, \emph{eg} \(\sigma_{380} \approx \sqrt{G\, adu380 + V}\), where \(V\) is the product of the CCD chip gain squared by the number of pixels in the ROI by the CCD chip read-out variance. The algebric operations leading to the estimator (top right) are explicitely displayed. The paper explains how to compute the standard deviation of the derived distributions obtained at each step of the calcium concentration estimation.}
\end{center}


\textbf{Method name} Standard error for the ratiometric calcium estimator

\textbf{Keywords} Calcium measurements, Fura-2, propagation of uncertainty, propagation of errors, Monte-Carlo method, reproducible research.

\section{Method overview}
\label{sec:orge3f8326}

\subsection{Rational}
\label{sec:org2e754fa}
Since its introduction by \cite{grynkiewicz.poenie.ea:85}, the ratiometric indicator Fura-2 has led to a revolution in our understanding of the role of calcium ions (Ca\(^{2+}\)) in neuronal and cellular function. This indicator provides a straightforward estimation of the free Ca\(^{2+}\) concentration ([Ca\(^{2+}\)]) in neurons and cells with a fine spatial and time resolution. The experimentalist must determin a 'region of interest' (\texttt{ROI}) within which the [Ca\(^{2+}\)] can be assumed uniform and is scientifically relevant. Fluorescence must be measured following excitation at two different wavelengths: typically around 340 and 380 nm; and, since cells exhibit autofluorescence or 'background fluorescence' at those wavelengths, the measured fluorescence intensity is made of two sources: the Fura-2 linked fluorescence and the autofluorescence. The measured intensity within the ROI is therefore usually corrected by subtrating from it an estimation of the  autofluorescence intensity obtained from simultaneous measurements from a 'background measurement region' (\texttt{BMR}); that is, a nearby region where there is no Fura-2. At a given time the experimentalist will therefore collect a fluorescence intensity measurement from the ROI at 340 and 380 nm; we are going to write \texttt{adu340} and \texttt{adu380} these measurements, where 'adu' stands for 'analog to digital unit' and correspond to the raw output of the fluorescence measurement device, most frequently a charge-coupled device (\texttt{CCD}). The experimentalist will also collect intensity measurements from the BMR, measurements that we are going to write \texttt{adu340B} and \texttt{adu380B}. If \texttt{P} CCD pixels make the ROI and \texttt{P\_B} pixels make the BMR and if the illumination time at 340 nm is \texttt{T\_340}, while the illumination time at 380 nm is \texttt{T\_380} (both times are measured in \(s\)), the experimentalist starts by estimating the fluorescence intensity per pixel per time unit following an excitation by a light flash of wavelengths \(\lambda\) (\(\lambda = 340\) or \(380\) nm) as:
\begin{equation}\label{eq:fluo_est_at_lambda}
  f_{\lambda} = \frac{1}{\text{T}_{\lambda}} \left(\frac{\text{adu}\lambda}{\text{P}} - \frac{\text{adu}\lambda\text{B}}{\text{P}_\text{B}}\right)\, , \text{ with } \lambda = 340\text{ or } 380 \text{ nm}\, , 
  \end{equation}
where an assumption of autofluorescence uniformity is implicitly made. The following ratio is then computed:
  \begin{equation}\label{eq:ratio_est}
  r = \frac{f_{340}}{f_{380}}\, .
\end{equation}
This is an important and attractive feature of the method as well as the origin of its name. Since only ratios are subsequently used, geometric factors like the volume of the Fura loaded region under the ROI do not need to be estimated. 

The \emph{estimated} [Ca\(^{2+}\)] that we will write \(\widehat{\text{Ca}}\) for short (the '\(\widehat{\quad}\)' sign is used for marking estimated values) is then obtained, following \cite[Eq. 5, p. 3447]{grynkiewicz.poenie.ea:85}, with:
\begin{equation}\label{eq:RatiometricEstimator}
  \widehat{\text{Ca}} = K_{eff} \, \frac{r-R_{min}}{R_{max}-r} \, ,
\end{equation}
where \(K_{eff}\) (measured in \(\mu{}M\)), \(R_{min}\) and \(R_{max}\) are calibrated parameters (the last two parameters are ratios and are dimensionless).

If we now want to rigorously fit [Ca\(^{2+}\)] dynamics models to sequences of \(\widehat{\text{Ca}}\), we need to get \emph{standard errors}, \(\sigma_{\widehat{\text{Ca}}}\), on our estimates. This is where the ratiometric method gets 'more involved', at least if we want standard errors from a single transient as opposed to a mean of many transients. Despite the ubiquity of ratiometric measurements in neuroscience and cell physiology, we are aware of a single paper--by some of us \cite{Joucla_2010}--where the 'standard error question' was directly addressed. The method proposed in \cite{Joucla_2010} requires a 3 wavelengths protocol: measurements at 340, 380 \emph{and} 360 (the isosbestic wavelength) nm; it drops, so to speak,  the above advantage of working with a ratiometric estimator since it fits directly the adu340 and adu380 data (at the cost of estimating some geometry related parameters) and it requires a model of the autofluorescence dynamics if the latter is not stationnary. It therefore requires a slightly more complicated '3 wavelengths' recording protocol as well as a more involved fitting procedure. The dataset of the companion paper exhibits a clear but reversible autofluorescence rundown that cannot be ignored since autofluorescence accounts for half of the signal in the ROI. Rather that constructing / tailoring the accurate enough autofluorence models required by the 'direct approach' of \cite{Joucla_2010} we looked for an alternative method providing standard errors for the ratiometric estimator.      

\section{Ratiometric estimator variance}
\label{sec:orgab4d25f}

\subsection{Fluorescence intensity}
\label{sec:org55c1179}

As detailed in \cite{grynkiewicz.poenie.ea:85,Joucla_2010}, the fluorescence intensities giving rise to the adu340, adu340B, adu380 and adu380B signals can be written as:
\begin{align}
I_{340} &=  \left\{\frac{[Fura]_{total}\, \phi}{K_{Fura}+[Ca^{2+}]}\left(R_{min}\, K_{eff} + R_{max} [Ca^{2+}]\right) + F_{340B}\right\} \, T_{340} \, P  \, ,\label{eq:I340}\\
I_{340B} &=  F_{340B} \, T_{340} \, P_B  \, ,\label{eq:I340B}\\
I_{380} &=  \left\{\frac{[Fura]_{total}\, \phi}{K_{Fura}+[Ca^{2+}]}\left(K_{eff} + [Ca^{2+}]\right) + F_{380B}\right\} \, T_{380} \, P  \, , \label{eq:I380}\\
I_{380B} &=  F_{380B} \, T_{380} \, P_B  \, , \label{eq:I380B}
\end{align}
where \(F_{\lambda{}B}\) is the autofluorescence intensity per pixel per time unit at wavelength \(\lambda\), \(K_{Fura}\) is the Fura dissociation constant (a calibrated parameter measured in \(\mu{}M\)),
\([Fura]_{total}\), is the total (bound plus free) concentration of
Fura in the cell (measured in \(\mu{}M\)) and \(\phi\) is an experiment specific
parameter (measured in \(1/\mu{}M/s\)) lumping together the quantum efficiency, the neurite
volume, etc (see \cite{Joucla_2010} for details). When a '3 wavelengths protocol' is used (as was the case in the companion paper), it is easy to implement a self-consistency check based on these equations as explained in Sec. \ref{sec:orgb6850ac}.

\subsection{Recorded signals adu340, adu340B, adu380 and adu380B}
\label{sec:org17ba9e9}

As detailed and discussed in \cite{vliet.boddeke.ea:98,Joucla_2010}, the signal adu\(\lambda\) recorded with a CCD chip whose gain is \(G\) and whose read-out variance is \(\sigma^2_{read-out}\) can be modeled as the realization of a Gaussian random variable ADU\(\lambda\) with parameters:
\begin{align}
\mu_{ADU\lambda} &= G\, I_{\lambda}\, , \label{eq:mu_ADU_lambda}\\
\sigma^2_{ADU\lambda} &= G\, \mu_{ADU\lambda} + G^2\, P\, \sigma^2_{read-out}\, , \label{eq:s2_ADU_lambda}
\end{align}
with the obvious adaptation when dealing with the BMR signal: \(I_{\lambda}\) is replaced by \(I_{\lambda{}B}\) and \(P\) is replaced by \(P_B\).

\subsection{Variance estimates for adu340, adu340B, adu380 and adu380B}
\label{sec:orgb3f07fb}

So, to have the variance of \(ADU_{\lambda}\) we need to know \(I_{\lambda}\) and for that we need to know \([Ca^{2+}]\) (Eq. \ref{eq:I340} and \ref{eq:I380}) precisely what we want to estimate. But the expected value of \(ADU_{\lambda}\) is \(G\, I_{\lambda}\) (Eq. \ref{eq:mu_ADU_lambda}), we can therefore use as a first approximation the observed value \(adu_{\lambda}\) of \(ADU_{\lambda}\) as a guess for \(G\, I_{\lambda}\), so in Eq. \ref{eq:s2_ADU_lambda} we plug-in \(adu_{\lambda}\) for \(G\, I_{\lambda}\),  leading to:
\begin{equation}
\hat{\sigma}^2_{ADU_{\lambda}} = G\, adu_{\lambda} + G^2\, P \, \sigma^2_{read-out} \approx \sigma^2_{ADU_{\lambda}} \, . \label{eq:s2_ADU_lambda_approx}
\end{equation}  
In other words, we will use the observed \(adu_{\lambda}\) as if it were the actual fluorescence intensity times the CCD chip gain, \(ADU_{\lambda} = G \, I_{\lambda}\), in order to estimate the variance. In doing so we will sometime slightly underestimate the actual variance (when the observed \(adu_{\lambda}\) turns out to be smaller than \(ADU_{\lambda}\)) and sometime slightly overestimate it (when the observed \(adu_{\lambda}\) turns out to be larger than \(ADU_{\lambda}\)). Since we are going to combine many such approximations, we expect--and we will substantiate this claim in Sec. \ref{sec:orgefc3d97}--that overall the under-estimations will be compensated for by the over-estimations. 

\subsection{Variance estimate for \(\widehat{Ca}}\)}
\label{sec:orgd3919af}
Now that we have a \(\hat{\sigma}^2_{ADU_{\lambda}}\) we can work with--that is, an estimate from the data alone--, we want to get \(\hat{\sigma}^2_{r}\) (Eq. \ref{eq:ratio_est}) and \(\hat{\sigma}^2_{\widehat{Ca}}\). We can either use the \href{https://en.wikipedia.org/wiki/Propagation\_of\_uncertainty}{propagation of uncertainty} (also referred to as \emph{error propagation}, \emph{compounding of errors} or \emph{delta method}) \cite{rice:07,wilson:12} together with Eq. \ref{eq:ratio_est} and \ref{eq:RatiometricEstimator}, or a 'quick' Monte Carlo approach. We drop any explicit time index in the sequel in order to keep the equations more readable, but it should be clear that such variance estimates have to be obtained for each sampled point. 

\subsubsection{Propagation of uncertainty}
\label{sec:orgcd8e630}
This method requires, in the general case, an assumption of 'small enough' standard error since it is based on a first order Taylor expansion (see Sec. \ref{sec:org924e8a8} for details). It leads first to the following expression for the variance, \(\hat{\sigma}^2_{f_{\lambda}}\), of \(f_{\lambda}\) in Eq. \ref{eq:fluo_est_at_lambda}:
\begin{equation}\label{eq:fluo_var_at_lambda}
\hat{\sigma}^2_{f_{\lambda}} \approx \frac{1}{T_{\lambda}^2}\, \left(\frac{\hat{\sigma}^2_{ADU_{\lambda}}}{P^2} + \frac{\hat{\sigma}^2_{ADU_{\lambda}B}}{P_B^2}\right) \, .
\end{equation}
The variance \(\hat{\sigma}^2_{r}\) of \(r\) in Eq. \ref{eq:ratio_est} is then:
\begin{equation}\label{eq:ratio_var}
\hat{\sigma}^2_{r} \approx \frac{1}{f_{380}^2} \, (\hat{\sigma}^2_{f_{340}} + r^2 \, \hat{\sigma}^2_{f_{380}})
\end{equation}
and the variance \(\hat{\sigma}^2_{\widehat{Ca}}\) of \(\widehat{Ca}\) in Eq. \ref{eq:RatiometricEstimator} is:
\begin{equation}\label{eq:RatiometricEstimator_var}
\hat{\sigma}^2_{\widehat{Ca}} \approx \left(\frac{K_{eff}}{R_{max}-r}\right)^2 \, (1 + \widehat{Ca})^2 \,  \hat{\sigma}^2_{r} \, .
\end{equation}

\subsubsection{Monte-Carlo method}
\label{sec:orgf6c59b4}
Here we draw, \(k\) quadruple of vectors \[\left(adu_{340}^{[j]},adu_{340B}^{[j]},adu_{380}^{[j]},adu_{380B}^{[j]}\right)\, , \quad j=1,\ldots,k\, ,\] from four independent Gaussian distributions of the general form:
\begin{equation}\label{eq:ADUapproxVarMC}
adu_{\lambda}^{[j]} = adu_{\lambda} + z_{\lambda}^{[j]}\, \hat{\sigma}_{ADU_{\lambda}}\, ,
\end{equation}
where \(adu_{\lambda}\) is the observed value and \(z_{\lambda}^{[j]}\) is drawn from a standard normal distribution. We then  
plug-in these quadruples into Eq. \ref{eq:fluo_est_at_lambda} leading to \(k\) couples:
\begin{align*}
f_{340}^{[j]} &= \frac{1}{\text{T}_{340}} \left(\frac{adu_{340}^{[j]}}{\text{P}} - \frac{adu_{340B}^{[j]}}{\text{P}_\text{B}}\right)\, ,\\
f_{380}^{[j]} &= \frac{1}{\text{T}_{380}} \left(\frac{adu_{380}^{[j]}}{\text{P}} - \frac{adu_{380B}^{[j]}}{\text{P}_\text{B}}\right)\,, \quad j=1,\ldots,k \, .
\end{align*}
These \(k\) couples are 'plugged-in Eq. \ref{eq:ratio_est}' leading to \(k\) \(r^{[j]}\):
\begin{equation*}
r^{[j]} = \frac{f_{340}^{[j]}}{f_{380}^{[j]}}\, \quad j=1,\ldots,k\, ,
\end{equation*}
before plugging in the latter into Eq. \ref{eq:RatiometricEstimator} to get \(k\) \(\widehat{Ca}^{[j]}\):
\begin{equation*}
\widehat{Ca}^{[j]} = K_{eff} \frac{r^{[j]}-R_{min}}{R_{max}-r^{[j]}}\, \quad j=1,\ldots,k\, .
\end{equation*}
The empirical variance of these simulated observations will be our \(\hat{\sigma}^2_{\widehat{Ca}}\):
\begin{equation}\label{eq:Ca_hatVarMC}
\hat{\sigma}^2_{\widehat{Ca}} = \frac{1}{k-1} \sum_{j=1}^{k}(\widehat{Ca}^{[j]}-\widehat{Ca}_{\bullet})^2\, , \quad \text{ where } \quad \widehat{Ca}_{\bullet} = \frac{1}{k} \sum_{j=1}^{k}\widehat{Ca}^{[j]} \, . 
\end{equation}
Since the Monte-Carlo method requires milder assumptions (the variances do not have to be small) and is easy to adapt, we tend to favor it, but both methods are working. 

\subsection{Comment}
\label{sec:orgf05d985}
The present approach based on a \(\hat{\sigma}^2_{\widehat{Ca}}\) estimation is slightly less rigorous than the 'direct approach' of \cite{Joucla_2010} but it is far more flexible since it does not require an independent estimation / measurement of \([Fura]_{total}\). In the companion paper we also chose to consider the calibrated parameters \(K_{eff}\), \(R_{min}\) and \(R_{max}\) as known, while only an estimation (necessarily imprecise since the calibration procedure is subject to errors) is known. But the same batch of Fura was used for all experiments that should then all exhibit the same (systematic) error. Since in the companion paper, we are not trying to get the exact standard error of the calcium dynamics parameters but to show difference between two protocols ('classical whole-cell' versus beta-escin perforated whole-cell), ignoring the uncertainty on the calibrated parameters makes our estimates less variable and helps making the difference, if it exists, clearer. For a true calcium buffering capacity study, the errors on the calibrated parameters should be accounted for and it would be straightforward to do it with our approach, simply by working out the necessary derivatives if one works with the propagation of uncertainty method, or by drawing also \(K_{eff}\), \(R_{min}\) and \(R_{max}\) values from Gaussian distributions centered on their respective estimates with a SD given by the standard errors if one works with the Monte-Carlo approach.



\section{Empirical validation}
\label{sec:orgefc3d97}
\subsection{Rational}
\label{sec:org59b8acf}

Equations \ref{eq:I340}, \ref{eq:I340B}, \ref{eq:I380}, \ref{eq:I380B},  together with  Eq. \ref{eq:mu_ADU_lambda} and \ref{eq:s2_ADU_lambda} can be viewed as a data generation model. This means that if we choose model parameters values as well as an arbitrary [Ca\(^{2+}\)] time course, we can simulate measurements (\texttt{adu}) at both wavelengths in the ROI as well as in the BMR. We can then use these simulated \texttt{adu} exactly as we used the actual data, namely get \(r(t_i)\) (Eq. \ref{eq:ratio_est}) and \(\widehat{Ca}(t_i)\) (Eq. \ref{eq:RatiometricEstimator}) as well as the (squared) standard errors \(\hat{\sigma}^2_{\widehat{Ca}}(t_i)\) (Sec. \ref{sec:orgd3919af}).

Now if the \(\hat{\sigma}^2_{\widehat{Ca}}(t_i)\) are good approximations for the actual but unknown \(\sigma^2_{Ca}(t_i)\), the distribution of the \emph{normalized residuals}: $$\frac{\widehat{Ca}(t_i)-Ca(t_i)}{\hat{\sigma}_{\widehat{Ca}}(t_i)}\, ,$$ should be very close to a standard normal distribution. \emph{This is precisely what we are going to check}.

\subsection{Simulated data}
\label{sec:org326a3d8}

We are going to use the first transient of dataset \texttt{DA\_121219\_E1} of the companion paper. The 'static' parameters--that is the parameters not link to the calcium dynamics--used for the simulation are the actual experimental parameters rounded to the third decimal (Table \ref{tbl:sim-para}). 

\vspace{0.25cm}
\begin{center}
\begin{tabular}{l l}
\hline
Parameter & Value \\
\hline
$R_{min}$ & 0.147\\
$R_{max}$ & 1.599\\
$K_{eff}$ & 1.093 ($\mu{}M$)\\
$K_{Fura}$ & 0.225 ($\mu{}M$)\\
$[Fura]_{total} \phi$ & 1.89e+05 ($s^{-1}$)\\
T_{340} & 0.01 (s)\\
T_{380} & 0.003 (s)\\
P & 3\\
P_{B} & 448\\
G & 0.146\\
$\sigma^2_{read-out}$ & 268.96\\
$F_{340B}$ & 189512  ($s^{-1}$)\\
$F_{380B}$ & 711589  ($s^{-1}$)\\
\hline
\end{tabular}
\captionof{table}{\label{tbl:sim-para}'Static' parameters used for the simulation.}
\end{center}
\vspace{0.25cm}

The simulated calcium dynamics is a monoexponential decay mimicking the tail of the transient: \[Ca(t) = Ca_0 + \left\{ \begin{array}{l r} 0 & \text{if } t < t_0\\ \delta \exp (-(t-t_0)/\tau) & \text{if } t \ge t_0\end{array}\] and the parameter values (Table \ref{tbl:Ca-para}) are just a rounded version of the fitting procedure output (see companion paper).

\vspace{0.25cm}
\begin{center}
\begin{tabular}{l l}
\hline
Parameter & Value \\
\hline
$t_{0}$ & 2283.415 (s)\\
$Ca_{0}$ & 0.059 ($\mu{}M$)\\
$\delta$ & 0.114 ($\mu{}M$)\\
$\tau$ & 2.339 (s)\\
\hline
\end{tabular}
\captionof{table}{\label{tbl:Ca-para}Calcium dynamics parameters used for the simulation. Time 0 is when seal is obtained.}
\end{center}
\vspace{0.25cm}

The simulated data obtained in that way are shown on Fig. \ref{fig:stim1-actual-and-simulated} (blue traces) together with the actual data (red curves) they are supposed to mimic. At a qualitative level at least, our data generation model is able to produce realistic looking simulations.

\begin{center}
\includegraphics[width=.9\linewidth]{figs/stim1-actual-and-simulated.png}
\captionof{figure}{\label{fig:stim1-actual-and-simulated} Observed (red) and simulated (blue) ADU at 340 (left) and 380 nm (right) for the first transient (only the late phase of the transient was simulated).}
\end{center}

\subsection{Software and simulation details}
\label{sec:org374957f}

The methodological details of the measurements to which the analysis presented in the present manuscript was applied are described in the companion paper.

The simulations, computations and figures of the present manuscript were done with \texttt{Python 3} (\url{https://www.python.org/}), \texttt{numpy} (\url{https://numpy.org/}),  \texttt{scipy} and \texttt{matplotlib} (\url{https://matplotlib.org/}). The \texttt{Python} codes and the data required to reproduce the simulations and figures presented in this manuscript can be downloaded from \texttt{GitLab} (\url{https://gitlab.com/c\_pouzat/getting-se-on-ratiometric-ca-estimator}).

The use of \texttt{scipy} was kept to a bar minimum to maximize code lifeduration (\texttt{scipy} tends to evolve too fast with minimal concern for backward compatibility). The random number generators used were therefore the ones of \texttt{numpy}: the uniform random number generator derives from the \texttt{Permuted Congruential Generator (64-bit, PCG64)} (\url{https://www.pcg-random.org/}) \cite{oneill:pcg2014} while the normal random number generator is an adaptation of the Ziggurat method \cite{Marsaglia_2000} of \texttt{Julia} (\url{https://docs.julialang.org/en/v1/}); unfortunately one has to check the source code of both \texttt{numpy} and \texttt{Julia} to find that out.  

\subsection{Are the standard errors of ratiometric estimator accurate?}
\label{sec:orge1a7030}

Since the two \(\hat{\sigma}^2_{\widehat{Ca}}\) estimation methods, propagation of uncertainty and Monte-Carlo, agree at each time point within 2\%, we illustrate in this section the results obtained with the Monte-Carlo method.

We take next the simulated data (blue curves on Fig. \ref{fig:stim1-actual-and-simulated}) together with the simulated background signals (not shown) as if they were actual data and we compute the ratiometric estimator and its standard error as described in Sec. \ref{sec:orgd3919af}, using \(k=10^4\) replicates. Figure \ref{fig:stim1-actual-and-ratiometric-Ca-plot} shows the standardized residuals as well as the simulated data together with the true [Ca\(^{2+}\)], we know it since we used it to simulate the data!

\begin{center}
\includegraphics[width=.9\linewidth]{figs/stim1-actual-and-ratiometric-Ca-plot.png}
\captionof{figure}{\label{fig:stim1-actual-and-ratiometric-Ca-plot} Top: Simulated ratiometric estimator - 'actual' [Ca\(^{2+}\)] divided by ratiometric estimator standard error (if everything goes well we should see draws from a standard normal distribution); bottom: Simulated ratiometric estimator (with error bars given by the standard error) in black and 'actual' [Ca\(^{2+}\)] in red.}
\end{center}

The upper part of Fig. \ref{fig:stim1-actual-and-ratiometric-Ca-plot} is only a qualitative way of checking that the normalized residuals follow a standard normal distribution. A quantitative assessment is provided by the Shapiro-Wilk \texttt{W} statistic, that is here: \texttt{0.987}; giving a p-value of \texttt{0.128}. There is therefore no ground for rejecting the null hypothesis that the normalized residuals are IID draws from a standard normal distribution.

As an additional, visual but less powerful test, we plot the empirical cumulative distribution function (ECDF) of the normalized residuals together with the theoretical (normal) one and with \href{https://en.wikipedia.org/wiki/Kolmogorov\%E2\%80\%93Smirnov\_test\#Kolmogorov\_distribution}{Kolmogorov's confidence bands} (Fig. \ref{fig:ECDF-normalized-residuals-sim-stim1}). If the empirical ECDF arises from a normally distributed sample with mean 0 and SD 1, it should be \emph{completely} contained in the 95\% \href{https://en.wikipedia.org/wiki/Confidence\_and\_prediction\_bands}{confidence band} 95\% of the time and in the 99\% band, 99\% of the time (these are \emph{confidence bands} not collections of pointwise confidence intervals).  

\begin{center}
\includegraphics[width=.9\linewidth]{figs/ECDF-normalized-residuals-sim-stim1.png}
\captionof{figure}{\label{fig:ECDF-normalized-residuals-sim-stim1} Empirical cumulative distribution function (ECDF) of the normalized residuals (red) together with 95\% (grey) and 99\% (blue) Kolomogorov confidence bands.}
\end{center}

We conclude from these visual representations and formal tests that our normalized residuals follow the expected standard normal distribution, implying that our proposed method for getting the standard errors of the ratiometric estimator is fundamentally correct.

\section{Discussion}
\label{sec:orge5ac178}

We have presented a new and simple method for getting standard errors on calcium concentration estimates from ratiometric measurements. This method does not require any more data than what experimentalists using ratiometric dyes like Fura-2 are usually collecting: measurements at 340 and 380 nm both within a region of interest and within a background measurement region. Once the errors bars have been obtained, arbitrary models can be fitted to the calcium transients--by weighted nonlinear least-squares \cite{IMM2010-05938}--and meaningful confidence intervals for the parameters of these models will follow as illustrated in the companion paper. The present contribution is therefore best viewed a major simplification of the 'direct approach' of \cite{Joucla_2010}. In contrast to the latter, the new method does not require a '3 wavelengths protocol', it does not require either a precise fit of the autofluorescence dynamics at the three wavelengths and is therefore much easier to implement. Such a 3 wavelengths protocol can nevertheless be useful since it allows experimentalists to perform self-consistency checks and to detect potentially wrong calibration parameter (Sec. \ref{sec:orgb6850ac}). We provide moreover two independent implementations, one in \texttt{C} and one in \texttt{Python}, they are open source and freely available. The rather verbose \texttt{Python} implementation of the heart of the method (Sec. \ref{sec:orgd3919af}) requires 25 lines of code and nothing beyond basic \texttt{numpy} functions. We are therefore confident that this method could help experimental physiologists getting much more quantitative results at a very modest extra cost. 

\printbibliography

\appendix

\section{A Self-consistency check}
\label{sec:orgb6850ac}
\subsection{Plug-in \(adu\lambda\) estimates}
\label{sec:org4d682fb}
If we have data where a '3 wavelengths protocol' was implemented (that is, flashes at 340, 360 and 380 nm at each time point were made), we can use a self consistency check of the data generation model defined by Eq. \ref{eq:I340}, \ref{eq:I380}, \ref{eq:ratio_est} and \ref{eq:RatiometricEstimator}. From the 6 measureed sequences:
\begin{itemize}
\item \(\{adu340(t_1),\ldots,adu340(t_n)\}\),
\item \(\{adu340B(t_1),\ldots,adu340B(t_n)\}\),
\item \(\{adu360(t_1),\ldots,adu360(t_n)\}\),
\item \(\{adu360B(t_1),\ldots,adu360B(t_n)\}\),
\item \(\{adu380(t_1),\ldots,adu380(t_n)\}\),
\item \(\{adu380B(t_1),\ldots,adu380B(t_n)\}\),
\end{itemize}
we compute the sequence of ratiometric calcium estimates \(\{\widehat{\text{Ca}}(t_1),\ldots,\widehat{\text{Ca}}(t_n)\}\). If we also have a way of getting \(\tilde{\phi}\) such that: \[\tilde{\phi} \tilde{I}_{360} \approx [Fura]_{total} \phi\, ,\] where \[\tilde{I}_{360} = \frac{1}{G\, T_{360}} \left(\frac{adu360}{P}-\frac{adu360B}{P_B}\right)\, ,\] \(\tilde{I}_{360}\) is as estimate of the fluorescence intensity per pixel, per  time unit due to Fura (we discuss in the next subsection how to obtain \(\tilde{\phi}\)), we will get:
\begin{itemize}
\item \(\{\widetilde{adu}340(t_1) = G\, \tilde{I}_{340}(t_1),\ldots,\widetilde{adu}340(t_n) = G \, \tilde{I}_{340}(t_n)\}\),
\item \(\{\widetilde{adu}380(t_1) = G \, \tilde{I}_{380}(t_1),\ldots,\widetilde{adu}380(t_n) = G \, \tilde{I}_{380}(t_n)\}\)
\end{itemize}
where \(\tilde{I}_{340}\) and \(\tilde{I}_{380}\) are 'plug-in' estimates of \(I_{340}\) and \(I_{380}\) obtained from Eq. \ref{eq:I340} and \ref{eq:I380} by plugging in \(\widehat{\text{Ca}}\):
\begin{align*}
\tilde{I}_{340}(t_i) & = \left\{\frac{\tilde{\phi} \tilde{I}_{360}(t_i)}{K_{Fura}+\widehat{\text{Ca}}(t_i)}\left(R_{min}\, K_{eff} + R_{max} \widehat{\text{Ca}}(t_i)\right) + \hat{F}_{340B}(t_i)\right\} \, T_{340} \, P  \, ,\\
\tilde{I}_{380}(t_i) & = \left\{\frac{\tilde{\phi} \tilde{I}_{360}(t_i)}{K_{Fura}+\widehat{\text{Ca}}(t_i)}\left(K_{eff} + \widehat{\text{Ca}}(t_i)\right) + \hat{F}_{380B}(t_i)\right\} \, T_{380} \, P  \, ,
\end{align*}
with \[\hat{F}_{\lambda{}B}(t_i) = \frac{adu\lambda{}B(t_i)}{G \, P_B \, T_{\lambda}} \, .\]
The \(\{(\widetilde{adu}340(t_i),\widetilde{adu}380(t_i))\}_{i=1,\ldots,n}\) should then be comparable to the original \linebreak \(\{(adu340(t_i),adu380(t_i))\}_{i=1,\ldots,n}\). We have here a self-consistency criterion, not a test since we are using the measured \(adu\lambda(t_i)\) (via \(\widehat{\text{Ca}}(t_i)\)) to get \(\widetilde{adu}\lambda(t_i)\).

\subsection{Estimating \(\tilde{\phi}\)}
\label{sec:org1b48cdc}

We introduce: \[RSS(\phi) = \sum_{i=1}^n\left(adu_{340}(t_i) - \widetilde{adu}_{340}(t_i)\right)^2 + \left(adu_{380}(t_i) - \widetilde{adu}_{380}(t_i)\right)^2\]
and define \(\tilde{\phi}\) as the argument minimizing \(RSS(\phi)\). We just need to solve a nonlinear least-square problem.

\subsection{Illustration}
\label{sec:orgf570c10}

As an illustration we compute the plug-in estimators in two situations both based on the first transient of dataset \texttt{DA\_121219\_E1}. In the first case we use the calibrated values for \(R_{min}\) and \(R_{max}\) (left column on Fig. \ref{fig:plug-in-illustration}). In the second case we use an \(R_{min}\) 20\% smaller than the calibrated value and an \(R_{max}\) 20\% larger (right column on Fig. \ref{fig:plug-in-illustration}).

\begin{center}
\includegraphics[width=.9\linewidth]{figs/plug_in_illustration.png}
\captionof{figure}{\label{fig:plug-in-illustration} Observed (red) and plug-in (blue) ADU at 340 (top) and 380 nm (bottom) for the first transient. On the left, the calibrated parameters were used while on the right an \(R_{min}\) underestimated by 20\% and and \(R_{max}\) overestimated by 20\% were used.}
\end{center}

We see that the plug-in estimator does indeed provide a self-consistency check (the plug-in estimator values follow closely the observed values on the left side of Fig. \ref{fig:plug-in-illustration}) and is sensitive to calibrated parameter deviations from their 'true' values (the peak / valleys 'plug-in' deviate from the observed values on the right side of Fig. \ref{fig:plug-in-illustration}).

\section{Propagation of uncertainty}
\label{sec:org924e8a8}
We outline in this section how to reach Eq. \ref{eq:fluo_var_at_lambda}, \ref{eq:ratio_var} and \ref{eq:RatiometricEstimator_var} of Sec. \ref{sec:orgcd8e630}. We first need to remember that is \(X\) and \(Y\) are two \emph{independent} random variables with mean \(\mathbb{E}X = \mu_X\) and \(\mathbb{E}Y = \mu_Y\) and variance \(\mathbb{V}X = \sigma^2_X\) and \(\mathbb{V}Y = \sigma^2_Y\), then if \(Z = a + b X + c Y\) (\(a,b,c \in \mathbb{R}\)) we have:
\begin{align*}
\mathbb{E}Z &= a + b\, \mu_X + c\, \mu_Y\, ,\\
\mathbb{V}Z &= b^2\, \sigma^2_X + c^2\, \sigma^2_Y\, .
\end{align*}
Eq. \ref{eq:fluo_var_at_lambda} is a direct consequence of the last equality.
If \(X\) is (approximately) normally distributed with (\(X \sim \mathcal{N}(\mu_X,\sigma^2_X)\)) as well as \(Y\), we can write: \(X \approx \mu_X + Z_1 \sigma_X\) and \(Y \approx \mu_Y + Z_2 \sigma_Y\), where \(Z_1\) and \(Z_2\) are independent and follow a standard normal distribution, \(\mathcal{N}(0,1)\).
If now \(Z = f(X,Y)\) and the partial derivatives of \(f\) at \((\mu_X,\mu_Y)\) exist then:
\begin{align*}
Z &= f(\mu_X+Z_1 \sigma_X,\mu_Y+Z_2 \sigma_Y) \\
& \approx f(\mu_X,\mu_Y) + Z_1 \sigma_X \frac{\partial f(\mu_X,\mu_Y)}{\partial X} + Z_2 \sigma_Y \frac{\partial f(\mu_X,\mu_Y)}{\partial Y}\, . 
\end{align*}
This is just a first order Taylor expansion and that is where the 'small enough standard error' assumption is necessary. \(Z\) is then (approximately) a linear combination of two independent standard normal random variables and we immediately get:
\begin{align*}
\mathbb{E}Z &= f(\mu_X,\mu_Y)\, ,\\
\mathbb{V}Z &= \left(\frac{\partial f(\mu_X,\mu_Y)}{\partial X}\right)^2\, \sigma^2_X + \left(\frac{\partial f(\mu_X,\mu_Y)}{\partial Y}\right)^2\, \sigma^2_Y\, .
\end{align*}
Eq. \ref{eq:ratio_var} follows directly by computing the necessary partial derivatives, while Eq. \ref{eq:RatiometricEstimator_var} requires the computation of a single derivative. 


\section{Auto-fluorescence dynamics}
\label{sec:org4622d12}
\subsection{General features}
\label{sec:orgad38e18}
The evolution of the \(adu\lambda{}B\) is shown on Fig. \ref{fig:general_autofluo_dynamics}. We see that the autofluorescence runs down when high frequency flashes are applied during the 3 transients, with a partial recovery between transients.

\begin{center}
\includegraphics[width=.9\linewidth]{figs/general_autofluo_dynamics.png}
\captionof{figure}{\label{fig:general_autofluo_dynamics} Autofluorescence at 3 excitation wavelengths, 340 nm (red), 360 (blue), 380 (brown). Both during low frequency stimaltions (four portions made of dots) and during the transients where a higher frequency stimulation was applied (3 groups with almost vertical lines).}
\end{center}

\subsection{Within transient dynamics}
\label{sec:org8dcc328}

The 'direct method' of \cite{Joucla_2010} requires the knowledge of the autofluorescence value at each time point during a transient at both 340, 360 and 380 nm, since Eq. \ref{eq:I340} and \ref{eq:I380} are fitted directly to the recorded \texttt{adu340} and \texttt{adu380} and they depend on the total Fura concentration at transient time that is estimated from the difference of the 360 nm measurements in the ROI and the BMR. We therefore take a closer look a the autfluorescence dyanmics during the first transient (Fig. \ref{fig:autofluo_dynamics_s1}).

\begin{center}
\includegraphics[width=.9\linewidth]{figs/autofluo_dynamics_s1.png}
\captionof{figure}{\label{fig:autofluo_dynamics_s1} Normalized autofluorescence at 3 excitation wavelengths, 340 nm (red), 360 nm (blue) and 380 (brown) during the first transient. At each wavelength, the normalization is performed by dividing each value by the maximal one.}
\end{center}

At that stage we can fit a straight line plus a cosine function whose period is the duration of a transient. That's a good way to capture the main structure in the transient, but is still does not account for the full signal variability (Fig. \ref{fig:aduB_s1_fit_and_data}). As can be seen from the normalized residuals--the residuals divided by the standard deviation--that should be very nearly independent random draws from a standard normal distribution if the model is correct, there are finer structures left (like the double valley on the 380 nm residuals) meaning that those fits won't pass formal goodness of fit tests. Indeed if we apply Pearson's \(\chi^2\) tests to these stabilized residuals we get:
\begin{itemize}
\item at 340 nm a residual sum of squares (RSS) \texttt{326}, leading to a \(\mathbb{P}(\chi^2_{197} > 326)=\) \texttt{0.0},
\item at 360 nm a RSS of \texttt{288}, leading to a \(\mathbb{P}(\chi^2_{197} > 288)=\) \texttt{2.6e-05},
\item at 380 nm a RSS of \texttt{275}, leading to a \(\mathbb{P}(\chi^2_{197} > 275)=\) \texttt{0.000203}.
\end{itemize}


\begin{center}
\includegraphics[width=.9\linewidth]{figs/aduB_s1_fit_and_data.png}
\captionof{figure}{\label{fig:aduB_s1_fit_and_data} Bottom: Autofluorescence (red) at 340 nm (left), 360 nm (middle) and 380 nm (right) together with a straight line plus cosine function fit (blue). Top, the normalized residuals: \((\text{adu} - \text{fit})/\sqrt{G \, \text{fit} + P_B \, G^2 \, \sigma^2_{read-out}}\).}
\end{center}

We are then left with three possibilities:
\begin{enumerate}
\item try to refine the 'straight line plus cosine function' empirical model in order to get acceptable fits,
\item try to get a better understanding of the autofluorescence dynamics,
\item find another way to get error bars on our estimates.
\end{enumerate}
Since we wanted to propose an 'as general and easy as possible' method we chose the third approach in the present manuscript. 

\end{document}