We thank the referee for carefully reading our manuscript and for making very useful suggestions. As detailed below, we agree with all the referees suggested changes and we have modified the mansucript accordingly. We hope that these changes will fully meet the referee's demands.

In the sequel, the page numbers refer to the PDF version where our changes are shown in blue.

----
In the second, third, fourth and fifth phrases the referee writes:
"However, it would benefit from a clarification of exactly when and why this approach might be preferable to using the more common direct calculation of error from multiple repetitions of experiments. The authors are not very explicit  
about it and it is not helped by the constant reference to a companion paper which I do not possess. I presume it is of value when fitting a dynamic model to the Ca time course from a single cell where the variability of the Ca signal is high due to low fluorescence emission levels. A weighted least squares fitting method is mentioned which requires knowledge of the standard error. I quite accept that but they should, for completeness, make a case as why one cannot simply use unweighted least squares and avoid the need to calculate standard error at all."

We explain now between the bottom of p. 6 and the top of p. 7 why we cannot repeat the measurements in the exact same conditions and why it is also of interest to use a single stimulation instead of many (when many can be used):
"We typically work (e.g. Joucla et al, 2010; Hess et al, 2021) in a setting, using the so called 'added buffer approach', where we cannot get more than a single transient in given conditions since Fura is constantly diffusing into the recorded cell modifying thereby the time constant of calcium transients. It is worth pointing out that there is a more general interest in obtaining standard errors from a single transient: getting these fluorescence measurements requires shinning UV light on the neurons we are recording from and generates photodamage."

We have added a new paragraph (p. 9) at the end of Sec. "Propagation of uncertainty" explaining qualitatively why the standard error of the ratiometric calcium estimator increases with the calcium concentration. This standard error inhomogeneity (or heteroscedasticity) forces us to us weighted nonlinear least squares estimation. We have added to our original reference to Nielsen and Madsen (2010), two classical nonlinear least square books (Bates and Watts, 1988 and Seber and Wild, 1989) where the non-weighted / weighted issue is comprehensively covered.  

We apologize for the constant reference to the "companion article". This is due to the history of the manuscript production. The present manuscript was originally a (big) appendix to the "companion manuscript" and the Cell Calcium editor suggested that we split the latter. The companion manuscript is now given proper reference since it has since then be published (Hess et al, 2021) and we hope that, following the referee's suggestions, our MethodsX manuscript can now be read essentially on its own.

----
The referee writes:
"P.6,para 1 ... 'aduxxx' variables. For consistency with the f and other variables it might be better to subscript the wavelength in the adu variables, e.g. adu<sub>380</sub> instead of adu380."

The wavelengths appear now as subscripts throughout the text.

----
The referee write next (same paragraph):
"Also the definition of 'adu' as 'the raw output of the fluorescence measurement  
device' is a bit imprecise. It should be made clear that 'adu' is the sum of the digital grey levels of the pixels with the defined regions of interest within the CCD sensor."

We now write on p. 6 after the first mention of the "analog to digital unit":
"if the experimentalist is careful not to saturate the sensor, the adu count is proportional to the number of photo-electrons present in the pixel, or in the group of pixels when on-chip binning is used, at the end of the exposure period."

----
In the next paragraph the referee writes:
"P.6 Eqn. (3) It would be useful to define the parameters Keff, Rmin, Rmax etc. in a little more detail here (e.g. Rmax is the maximum ratio in the presence of a saturating concentration of Ca) and also how the errors in estimation of these would lead to systematic rather than random errors."

On p. 6 after the first mention of the three calibrated parameters we now write:
"Rmin is the ratio (Eq. 2) observed in the absence of calcium, while Rmax is the ratio observed with a saturating concentration. Keff is the calcium concentration at which the ratio is half way between Rmin and Rmax. If a set of experiments is performed on a given cell type with the same batch of Fura, as in the companion paper (Hess et al, 2021), the calibration errors on these three parameters will be the same for each experiment. If different cell types are considered and/or different Fura batches are used, the calibration errors should be taken into account before making comparison of estimated calcium dynamics parameters (see Joucla et al, 2010 for discussion)."

----
In the next paragraph the referee writes:
"P.7 eqns. (8) and (9) … The CCD chip gain G seems to play a crucial role in the estimation of the standard error. If this is determined incorrectly then the error will be incorrect. The authors need to say more about how G is defined and experimentally determined. I presume it to be a property of the CCD and looking at eqn. (9) I infer that its units are adu per electron per pixel. I can see how one could readily obtain G from the camera specification and also how it might be experimentally determined from grey level variance vs mean curves from the CCD. Whatever the case, the author's should explain how it is determined."

We now write (p. 8):
"Parameters G and σ^2_{read-out} are CCD chip parameters provided by the manufacturer. Calibration procedures are discussed in (van Vliet et al, 1998 and Joucla et al, 2010} and a comprehensive exemple with data and codes can be found in (Pouzat, 2019). Our experience is that the values provided by manufacturers are good starting points; the user calibrated read-out noise is sometime slightly larger than the one specified by the manufacturer."

The added reference to Pouzat (2019) is a course material available on zenodo (https://zenodo.org/). It contains calibration data as well as both a Python and C version of the analysis leading to the two CCD parameter calibration.

----
In the next paragraph the referee writes:
"P.9 The authors mention that they 'tend to favor' the Monte-Carlo' method for computing the error in Ca over the propagation of errors (PoE) method because it works in large as well as small variance conditions, but both methods work. Can they be more explicit in their recommendation? How often are variances too high to safely using the PoE method?"

We now write (p. 10):
"... to be on the safe side, users can use both methods and, if they disagree, plot a histogram of the [sample of Monte Carlo ratiometric estimators] to make sure that the discrepancy source is the non-normality of the latter."

Fundamentally the error propagation method is less general but easier to implement (it can be used in the classroom with a hand calculator).

----
In the next paragraph the referee writes:
"P.9 Comment … I found this paragraph slightly confusing mainly due to the reference to the companion paper which I have not read. I agree it is important and useful to consider the treatment of the errors in the Keff, etc. parameters. I'm not quite convinced of the statement though that they could be readily (or usefully) incorporated into the error model presented here. I'm not sure, for instance, whether, Rmin and Rmax, were determined separately in each cell and in any case are essentially fixed for each cell, unlike the photon variance which  varies with each time period and pixel. It might be better to simply make it clear that these parameters are treated as fixed, introducing systematic rather than random error."

We agree and we have removed this confusing part. The issue of the uncertainty on the calibrated parameters is now, as mentioned above, briefly discussed on p. 6 after these parameters appear for the first time.

----
In the next paragraph the referee writes:
"P.15 Section A. I'm not sure about the usefulness of this section since it largely refers to a method described in a previous paper. It could be deleted since I don't think it adds much to this paper but It's not something I feel strongly about."

This section of the Appendix has been removed as well as the two references to it in the main text.

----
In the last paragraph the referee writes:
"Figure 1 legend. This is much too verbose with too much discussion which should properly be in the text. It should be rewritten using the proper minimally descriptive conventions for a figure legend."

This is for us the "hardest" referee's request. We have removed three phrases from this figure legend. But we fear that shortening it further would make it hard to understand for readers not very familiar with ratiometric measurements.
